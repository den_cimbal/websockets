import { prepareToGame, endGame, showWinners } from "./helpers/gameHelper.mjs";

const username = sessionStorage.getItem("username");

const roomsPage = document.getElementById("rooms-page");
const gamePage = document.getElementById("game-page");
const roomsList = document.getElementById("rooms-list");
const createRoom = document.getElementById("add-room-btn");
const quitButton = document.getElementById("quit-room-btn");
const usersList = document.getElementById("users-list");
const readyButton = document.getElementById("ready-btn");
const countdown = document.getElementById("countdown");
const timer = document.getElementById("timer");
const text = document.getElementById("text-container");
const closeButton = document.getElementById("quit-results-btn");
const resultsContainer = document.getElementById("results");

let roomName = null;

if (!username) {
  window.location.replace("/login");
}

const socket = io("", { query: { username } });

const userExistError = (error) => {
  alert(error);
  sessionStorage.clear();
  window.location.replace("/login");
};

const joinToTheRoom = (room) => {
  roomName = room.roomName;
  roomsPage.classList.add("display-none");
  gamePage.classList.remove("display-none");

  const roomTitle = document.getElementById("room-title");
  const usersFragment = document.createDocumentFragment();

  roomTitle.innerHTML = room.roomName;
  readyButton.innerHTML = "Ready";
  usersList.innerHTML = "";
  countdown.innerHTML = "";
  timer.innerHTML = "";
  text.innerHTML = "";

  for(let i = 0; i < room.users.length; i++) {
    const { progress, isReady } = room.users[i];
    const userWrapper = document.createElement("li");
    const userName = document.createElement("p");
    const progressBarWrapper = document.createElement("div");
    const progressBar = document.createElement("div");

    userName.innerHTML = username === room.users[i].username ? `${room.users[i].username} (you)` : room.users[i].username;
    userWrapper.classList.add("user");
    userName.classList.add("user-title");
    progressBarWrapper.classList.add("progress-wrapper");
    progressBar.classList.add("progress-bar", `${room.users[i].username.replace(/\s/g, '-')}`);
    progressBar.style.width = `${progress}%`;

    if (progress == 100) {
      progressBar.classList.add("progress-bar--full");
    }

    progressBarWrapper.append(progressBar);

    if (isReady) {
      userWrapper.classList.remove("ready-status-red");
      userWrapper.classList.add("ready-status-green");
    } else {
      userWrapper.classList.add("ready-status-red");
      userWrapper.classList.remove("ready-status-green");
    }

    userWrapper.append(userName, progressBarWrapper);
    usersFragment.append(userWrapper);
  }

  usersList.append(usersFragment);

  readyButton.classList.remove("display-none");
  text.classList.add("display-none");
};

const updateRooms = (rooms) => {
  const fragment = document.createDocumentFragment();

  roomsList.innerHTML = "";

  for(let i = 0; i < rooms.length; i++) {
    const { roomName, users } = rooms[i];
    const usersQuantity = users.length;
    const roomWrapper = document.createElement("div");
    const roomUsers = document.createElement("p");
    const roomTitle = document.createElement("h3");
    const roomJoinButton = document.createElement("button");

    roomWrapper.classList.add("room");
    roomJoinButton.classList.add("join-btn", "button");

    roomUsers.innerHTML = `${usersQuantity} user${usersQuantity > 1 ? 's' : ''} connected`;
    roomTitle.innerHTML = roomName;
    roomJoinButton.innerHTML = "Join";

    roomJoinButton.addEventListener("click", () => {
      socket.emit("JOIN_ROOM", roomName);
    });

    roomWrapper.append(roomUsers, roomTitle, roomJoinButton);
    fragment.append(roomWrapper);
  }

  roomsList.append(fragment);
};

const handleError = (error) => {
  alert(error);
};

const updateUserList = (users) => {
  const usersFragment = document.createDocumentFragment();

  usersList.innerHTML = "";

  for(let i = 0; i < users.length; i++) {
    const { progress, isReady } = users[i];
    const userWrapper = document.createElement("li");
    const userName = document.createElement("p");
    const progressBarWrapper = document.createElement("div");
    const progressBar = document.createElement("div");

    userName.innerHTML = username === users[i].username ? `${users[i].username} (you)` : users[i].username;
    userWrapper.classList.add("user");
    userName.classList.add("user-title");
    progressBarWrapper.classList.add("progress-wrapper");
    progressBar.classList.add("progress-bar", `${users[i].username.replace(/\s/g, '-')}`);
    progressBar.style.width = `${progress}%`;

    if (progress == 100) {
      progressBar.classList.add("progress-bar--full");
    }

    progressBarWrapper.append(progressBar);

    if (isReady) {
      userWrapper.classList.remove("ready-status-red");
      userWrapper.classList.add("ready-status-green");
    } else {
      userWrapper.classList.add("ready-status-red");
      userWrapper.classList.remove("ready-status-green");
    }

    userWrapper.append(userName, progressBarWrapper);
    usersFragment.append(userWrapper);
  }

  usersList.append(usersFragment);
}

const updateUserStatus = (changedStatus) => {
  readyButton.innerHTML = changedStatus ? "Not ready" : "Ready";
}

socket.on("USER_EXIST", userExistError);
socket.on("JOIN_TO_THE_ROOM", joinToTheRoom);
socket.on("UPDATE_ROOMS", updateRooms);
socket.on("JOIN_TO_THE_FULL_ROOM", handleError);
socket.on("JOIN_TO_THE_ACTIVE_ROOM", handleError);
socket.on("ROOM_EXIST", handleError);
socket.on("UPDATE_USERS", updateUserList);
socket.on("UPDATE_USER_STATUS", updateUserStatus);

socket.on("GAME_READY", ({ timer, roomName, textId, gameTime }) =>
  prepareToGame(timer, roomName, textId, gameTime, socket)
);

socket.on("GAME_END", (updatedRoom) => endGame(updatedRoom));

socket.on("SHOW_WINNERS", (updatedRoom) => showWinners(updatedRoom));

createRoom.addEventListener("click", () => {
  let roomName = prompt("Enter room name");

  if(roomName === null) {
    return false;
  }

  if(roomName.length > 0) {
    roomName = roomName.trim();
    socket.emit("CREATE_ROOM", roomName);
  } else {
    alert("Room name can\'t be empty");
  }
});

quitButton.addEventListener("click", () => {
  socket.emit("LEAVE_ROOM", roomName);
  gamePage.classList.add("display-none");
  roomsPage.classList.remove("display-none");
});

readyButton.addEventListener("click", () => {
  socket.emit("CHANGE_READY_STATUS", roomName);
});

closeButton.addEventListener("click", () => {
  resultsContainer.classList.remove("display-flex");
  resultsContainer.classList.add("display-none");
});