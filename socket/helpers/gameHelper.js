import { Game } from "./gameSettings";

let games = [];

export const getGame = (games, name) => {
  return games.filter((game) => game.name == name && !game.isGameEnd)[0];
};

export const getTextId = (sumOfTexts) => {
  const min = 0;
  const max = sumOfTexts - 1;
  let randomTextIndex = min + Math.random() * (max + 1 - min);
  return Math.floor(randomTextIndex);
};

export const sendGameConfig = ({ timer, name, textId, time, io }) => {
  io.in(name).emit("GAME_READY", { timer, roomName: name, textId, gameTime: time });
};

export const updatePlayerProgress = ({ players, username, progress }) => {
  const updatedPlayers = players.map((user) => {
    if (user.username === username) {
      const updatedUser = { ...user, progress };
      return updatedUser;
    }
    return user;
  });
  return updatedPlayers;
};

export const removePlayerFromGame = ({ players, username }) => {
  return players.filter((player) => player.username !== username);
};

export const sendPlayersProgress = ({ name, players, io }) => {
  io.in(name).emit("UPDATE_USERS", players);
};

export const checkIsGameCanEnd = (players) => {
  const finishedPlayers = players.filter((player) => player.progress === 100);
  return finishedPlayers.length == players.length;
};

export const checkIsGameCanStart = (room, minUsersForGameStart) => {
  const readyUsers = room.users.filter((user) => user.isReady);
  return readyUsers.length >= minUsersForGameStart && readyUsers.length === room.users.length;
};

export const createWinnerList = ({ players, finishedPlayers }) => {
  const notFinishedPlayers = players.filter((player) => player.progress !== 100);
  notFinishedPlayers.sort((a, b) => b.progress - a.progress);

  const finishedPlayersList = finishedPlayers.map((player) => `${player.username}`);
  const notFinishedPlayersList = notFinishedPlayers.map((player) => `${player.username}`);

  const winnerList = [...finishedPlayersList, ...notFinishedPlayersList].map((player, index) => {
    return `<p id="place-${index + 1}">${index + 1}. ${player}</p>`;
  });
  return winnerList.slice(0, 4).join("");
};

export const updateGameUsers = (roomName, username) => {
  const game = getGame(games, roomName);
  game.disconnectUser(username);
};

export const endEmptyGame = (roomName) => {
  games = games.map((game) => {
    if (game.name === roomName) {
      return { ...game, isGameEnd: true };
    }
    return game;
  });
};

export const createNewGame = (room, io, socket) => {
  const gameStartRoom = { ...room, isGameStart: true };
  const { roomName, users } = gameStartRoom;
  const newGame = new Game(roomName, users, io, socket);
  games.push(newGame);
  newGame.prepare();
  return { isRoomUpdated: true, room: gameStartRoom };
};

export const endCurrentGame = ({ roomName, updatedRoom, io, winners }) => {
  io.in(roomName).emit("SHOW_WINNERS", winners);
  io.in(roomName).emit("GAME_END", updatedRoom);
};

export const updateGameProgress = (roomName, username, updatedProgress, charsLeft) => {
  const game = getGame(games, roomName);
  game.updateProgress({ username, progress: updatedProgress, charsLeft });
};