import * as config from "../config";
import { removeUserFromRoom } from "./userHelper";
import { updateGameUsers, endEmptyGame, checkIsGameCanStart, createNewGame } from "./gameHelper";

let rooms = [];

export const createRoom = (roomName, userName, socket, io) => {
    const isRoomExist = (() => {
        const roomIndex = rooms.findIndex((room) => room.roomName === roomName);
        const isRoomExist = roomIndex !== -1;

        return isRoomExist;
    })();

    if(isRoomExist) {
        socket.emit("ROOM_EXIST", `Room with name ${roomName} already created. Please change the room name.`);
    } else {
        socket.join(roomName);
        const userData = {
            username: userName,
            isReady: false,
            progress: 0
        }
        const newRoom = { roomName, users: [userData], isGameStarted: false };

        socket.emit("JOIN_TO_THE_ROOM", newRoom);
        rooms.push(newRoom);
        io.emit("UPDATE_ROOMS", rooms);
    }
}

export const joinToTheRoom = (roomName, userName, socket, io) => {
    const roomIndex = rooms.findIndex((room) => room.roomName === roomName);
    const currentRoom = rooms[roomIndex];
    const isRoomAvailable = (() => {
        const isRoomFull = currentRoom.users.length >= config.MAXIMUM_USERS_FOR_ONE_ROOM;
        const isGameStart = currentRoom.isGameStarted;

        if(isRoomFull) {
            socket.emit("JOIN_TO_THE_FULL_ROOM", "Current room is full. Please choose another room.");
            return false;
        }

        if(isGameStart) {
            socket.emit("JOIN_TO_THE_ACTIVE_ROOM", "Game in current room is started already. Please choose another room.");
            return false;
        }

        return true;
    })();

    if(isRoomAvailable) {
        const userData = {
            username: userName,
            isReady: false,
            progress: 0
        }

        const users = [...currentRoom.users, userData];
        const updatedRoom = { ...currentRoom, users };

        socket.join(updatedRoom.roomName);
        socket.emit("JOIN_TO_THE_ROOM", updatedRoom);
        socket.to(roomName).emit("UPDATE_USERS", updatedRoom.users);

        // Refresh rooms
        const isRoomEmpty = !updatedRoom.users.length;
        let updatedRooms = null;
        if(isRoomEmpty) {
            updatedRooms = (() => rooms.filter((room) => room.roomName !== updatedRoom.roomName))();
        } else {
            updatedRooms = (() => {
                return rooms.map(room => {
                    if (room.roomName === updatedRoom.roomName) {
                        return updatedRoom;
                    }
                    return room;
                });
            })();
        }

        rooms = [...updatedRooms];
        io.emit("UPDATE_ROOMS", rooms);
    }
}

export const leaveRoom = (roomName, userName, socket, io) => {
    const roomIndex = rooms.findIndex((room) => room.roomName === roomName);
    const currentRoom = rooms[roomIndex];

    const users = currentRoom.users.filter((user) => user.username !== userName);
    const updatedRoom = { ...currentRoom, users };
    socket.leave(currentRoom);
    const isRoomEmpty = !currentRoom.users.length;

    if(!isRoomEmpty) {
        socket.to(roomName).emit("UPDATE_USERS", updatedRoom.users);
    }

    const isUpdatedRoomEmpty = !updatedRoom.users.length;
    let refreshedRooms = null;
    if(isUpdatedRoomEmpty) {
        refreshedRooms = (() => rooms.filter((room) => room.roomName !== updatedRoom.roomName))();
    } else {
        refreshedRooms = (() => {
            return rooms.map((room) => {
                if (room.roomName === updatedRoom.roomName) {
                    return updatedRoom;
                }
                return room;
            });
        })();
    }

    rooms = [...refreshedRooms];
    io.emit("UPDATE_ROOMS", rooms);
}

export const changeReadyStatus = (roomName, userName, socket, io) => {
  const roomIndex = rooms.findIndex((room) => room.roomName === roomName);
  const currentRoom = rooms[roomIndex];

  let changedStatus;
  const users = currentRoom.users.map(user => {
    if (user.username === userName) {
      changedStatus = !user.isReady;
      const updatedUser = { ...user, isReady: changedStatus };
      return updatedUser;
    }
    return user;
  });

  io.in(currentRoom.roomName).emit("UPDATE_USERS", users);
  const updatedRoom = { ...currentRoom, users };

  const isGameCanStart = checkIsGameCanStart(updatedRoom, config.MINIMUM_USERS_FOR_GAME_START);

  if (isGameCanStart) {
    createNewGame(updatedRoom, io, socket);
  } else {
    socket.emit("UPDATE_USER_STATUS", changedStatus);

    const isUpdatedRoomEmpty = !updatedRoom.users.length;
      let refreshedRooms = null;
      if(isUpdatedRoomEmpty) {
          refreshedRooms = (() => rooms.filter((room) => room.roomName !== updatedRoom.roomName))();
      } else {
          refreshedRooms = (() => {
              return rooms.map((room) => {
                  if (room.roomName === updatedRoom.roomName) {
                      return updatedRoom;
                  }
                  return room;
              });
          })();
      }

      rooms = [...refreshedRooms];
      io.emit("UPDATE_ROOMS", rooms);
  }
}

export const showRooms = (socket) => {
  socket.emit("UPDATE_ROOMS", rooms);
}

export const updateLeavingRooms = (userName, socket, io) => {
  const room = rooms.filter((room) => {
    const userIndex = room.users.findIndex((user) => user.username === userName);
    return userIndex !== -1;
  });

  if (!room.length) {
    return false;
  }

  const updatedRoom = removeUserFromRoom(room[0], userName);
  const isRoomEmpty = !updatedRoom.users.length;

  let refreshedRooms = null;
  let isUpdated = false;

  if(isRoomEmpty) {
    refreshedRooms = (() => rooms.filter((room) => room.roomName !== updatedRoom.roomName))();
  } else {
    refreshedRooms = (() => {
      return rooms.map((room) => {
          if (room.roomName === updatedRoom.roomName) {
              return updatedRoom;
          }
          return room;
      });
    })();

    isUpdated = true;
  }

  rooms = [...refreshedRooms];

  if(isUpdated) {
    socket.to(room[0].roomName).emit("UPDATE_USERS", updatedRoom.users);
    updateGameUsers(room[0].roomName, userName);
  }

  endEmptyGame(updatedRoom.roomName);
  io.emit("UPDATE_ROOMS", rooms);
}

export const updateRoom = (rooms, updatedRoom) => {
  return rooms.map((room) => {
    if (room.roomName === updatedRoom.roomName) {
      return updatedRoom;
    }
    return room;
  });
};

export const deleteRoom = (rooms, { roomName }) => {
  return rooms.filter((room) => room.roomName !== roomName);
};

export const refreshRooms = (rooms, updatedRoom) => {
  const isRoomEmpty = !updatedRoom.users.length;
  if (isRoomEmpty) {
    const refreshedRooms = deleteRoom(rooms, updatedRoom);
    return { refreshedRooms, status: "deleted" };
  } else {
    const refreshedRooms = updateRoom(rooms, updatedRoom);
    return { refreshedRooms, status: "updated" };
  }
};

export const updateRooms = (updatedRoom, io) => {
  const { isRoomUpdated, room } = updatedRoom;
  if (!isRoomUpdated) {
    return;
  }
  const { refreshedRooms } = refreshRooms(rooms, room);
  rooms = [...refreshedRooms];
  io.emit("UPDATE_ROOMS", rooms);
};

export const toDefaultRoom = ({ roomName, users }) => {
  const updatedUsers = users.map((user) => {
    return { ...user, isReady: false, progress: 0 };
  });
  return { roomName, users: updatedUsers, isGameStart: false };
};