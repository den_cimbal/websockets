let users = [];

export const addUser = (username, id) => {
  username = username.trim();
  const isUserExists = users.findIndex(user => user.username === username);
  if (isUserExists !== -1) {
    return { error: `User with username '${username}' already playing.` };
  }

  const newUser = { username, id };
  users.push(newUser);
  return { error: null, users };
};

export const deleteUser = (id) => {
  users = users.filter(user => user.id !== id);
  return users;
};

export const removeUserFromRoom = (room, username) => {
  const users = room.users.filter(user => user.username !== username);
  const updatedRoom = { ...room, users };
  return updatedRoom;
};