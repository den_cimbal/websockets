import {
  getTextId,
  sendGameConfig,
  updatePlayerProgress,
  sendPlayersProgress,
  removePlayerFromGame,
  checkIsGameCanEnd,
  createWinnerList,
  endCurrentGame
} from "./gameHelper";
import { toDefaultRoom } from "./roomHelpers";
import { SECONDS_TIMER_BEFORE_START_GAME, SECONDS_FOR_GAME } from "../config";
import { texts } from "../../data";
import { updateRooms } from "./roomHelpers";

export class Game {
  constructor(name, players, io, socket) {
    this.name = name;
    this.timer = SECONDS_TIMER_BEFORE_START_GAME;
    this.time = SECONDS_FOR_GAME;
    this.isGameEnd = false;
    this.players = players,
    this.finishedPlayers = [];
    this.winners = [];
    this.io = io;
    this.socket = socket;
    this.timeToEnd;
    this.charLeft;
  }

  prepare() {
    const textId = getTextId(texts.length);
    sendGameConfig({ timer: this.timer, name: this.name, textId, time: this.time, io: this.io });
    this.startTimer();
  }

  startTimer() {
    const updateTimeToStart = () => {
      this.timer--;
      if (this.timer < 0) {
        clearInterval(timeToStart);
        this.start();
      }
    };
    updateTimeToStart();
    const timeToStart = setInterval(updateTimeToStart, 1000);
  }

  start() {
    const updateTimeToEnd = () => {
      this.time--;
      if (this.time < 0) {
        this.endGame();
      }
    };
    updateTimeToEnd();
    this.timeToEnd = setInterval(updateTimeToEnd, 1000);
  }

  updateProgress({ username, progress }) {
    this.players = updatePlayerProgress({ name: this.name, players: this.players, username, progress });
    sendPlayersProgress({ name: this.name, players: this.players, io: this.io });
    const isPlayerFinished = progress === 100;
    if (isPlayerFinished) {
      const finishTime = SECONDS_FOR_GAME - this.time;
      this.finishedPlayers.push({ username, finishTime });
      this.checkGameStatus();
    }
  }

  disconnectUser(username) {
    if (username) {
      this.players = removePlayerFromGame({ players: this.players, username });
      this.checkGameStatus();
    }
  }

  checkGameStatus() {
    this.isGameEnd = checkIsGameCanEnd(this.players);
    if (this.isGameEnd) {
      this.endGame();
    }
  }

  endGame() {
    clearInterval(this.timeToEnd);
    this.winners = createWinnerList({
      players: this.players,
      finishedPlayers: this.finishedPlayers
    });
    const updatedRoom = toDefaultRoom({ roomName: this.name, users: this.players });
    endCurrentGame({ roomName: this.name, updatedRoom, io: this.io, winners: this.winners });
    setTimeout(() => {
      updateRooms({ isRoomUpdated: true, room: updatedRoom }, this.io);
    }, 3000);
  }
}
